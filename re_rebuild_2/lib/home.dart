import 'package:flutter/material.dart';
import 'package:re_rebuild_2/widget/detail.dart';
import 'package:re_rebuild_2/widget/near_you.dart';
import 'package:re_rebuild_2/widget/newly_added.dart';

class Home extends StatelessWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Location", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500, color: Colors.grey),),
                Row(
                  children: [
                    Image.asset('assets/maps.png', height: 20, width: 20, color: Colors.orange),
                    SizedBox(width: 5),
                    Text("Semarang", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),)
                  ],
                )
              ],
            ),
            Image.asset('assets/profile.png', height: 50, width: 50,)
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(left: 15, right: 15),
            child: Column(
              children: [
                SizedBox(height: 30),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 50,
                      width: widthScreen-90,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.grey[200]
                      ),
                      child: Padding(
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: Row(
                          children: [
                            Icon(Icons.search, size: 30, color: Colors.grey[400]),
                            SizedBox(width: 5),
                            Text("Search for kost", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500, color: Colors.grey[400]),),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.grey[200]
                      ),
                      child: Center(
                        child: Image.asset('assets/settings.png', height: 25, width: 25,),
                      )
                    ),
                  ],
                ),
                SizedBox(height: 35),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Near You", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                    Text("View  All", style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black54)),
                  ],
                ),
                SizedBox(height: 15),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      NearYou(imageUrl: 'assets/cottage.jpg', text: "\$20/mo", name: "Kost Anyar"),
                      NearYou(imageUrl: 'assets/house1.jpeg', text: "\$17/mo", name: "Kost Endi"),
                    ],
                  ),
                ),
                SizedBox(height: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Newly Added", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                    Text("View  All", style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black54)),
                  ],
                ),
                SizedBox(height: 15),
                Column(
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailHouse(imageUrl: 'assets/white.jpeg', name: "Kost Putih", text1: "Yogyakarta", text2: '\$18')));
                      },
                      child: NewlyAdded(imageUrl: 'assets/white.jpeg', name: 'Kontrakan Putih', text: '\$700', text2: '/year'),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailHouse(imageUrl: 'assets/grey.jpg', name: "Kost Angker", text1: "Yogyakarta", text2: '\$19')));
                      },
                      child: NewlyAdded(imageUrl: 'assets/grey.jpg', name: 'Kost Angker', text: '\$18', text2: '/month'),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailHouse(imageUrl: 'assets/rumah.jpg', name: "Kost Santri", text1: "Yogyakarta", text2: '\$17')));
                      },
                      child: NewlyAdded(imageUrl: 'assets/rumah.jpg', name: 'Kost Santri', text: '\$19', text2: '/month'),
                    ),
                  ],
                )
              ],
            ),
          )
        ),
      ),
    );
  }
}