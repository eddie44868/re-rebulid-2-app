import 'package:flutter/material.dart';

// ignore: must_be_immutable
class NearYou extends StatelessWidget {
  String imageUrl;
  String text;
  String name;
  NearYou({ Key? key, required this.imageUrl, required this.text, required this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 185,
          width: 240,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.grey[100]
          ),
          child: Column(
            children: [
              Stack(
                children: [
                  ClipRRect(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10.0), 
                        topRight: Radius.circular(10.0)),
                      child: Image.asset(
                        imageUrl,
                          fit: BoxFit.fill,
                      ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 15, top: 10, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 25,
                          width: 70,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.orange
                          ),
                          child: Center(
                            child: Text(text, style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500, color: Colors.white)),
                          )
                        ),
                        Container(
                          height: 25,
                          width: 70,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.blue
                          ),
                          child: Center(
                            child: Text("Featured", style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500, color: Colors.white)),
                          )
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(left: 15, top: 5, right: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(name, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black)),
                        SizedBox(height: 5),
                        Row(
                          children: [
                            Image.asset('assets/maps.png', height: 12, width: 12, color: Colors.orange),
                            SizedBox(width: 5),
                            Text("Semarang", style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500),)
                          ],
                        )
                      ],
                    ),
                    Image.asset('assets/heart.png', height: 25, width: 25),
                  ],
                )
              ),
            ],
          ),
        ),
        SizedBox(width: 20,)
      ],
    );
  }
}