import 'package:flutter/material.dart';

// ignore: must_be_immutable
class NewlyAdded extends StatelessWidget {
  String imageUrl;
  String text;
  String text2;
  String name;
  NewlyAdded({ Key? key, required this.imageUrl, required this.name, required this.text, required this.text2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return Column(
      children: [
        Container(
          height: 70,
          width: widthScreen-45,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.grey[100]
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 10, right: 10,),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.asset(
                          imageUrl,
                            height: 50,
                            width: 50,
                        ),
                    ),
                    SizedBox(width: 20,),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(name, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),),
                        SizedBox(height: 5),
                        Row(
                          children: [
                            Image.asset('assets/maps.png', height: 12, width: 12, color: Colors.orange),
                            SizedBox(width: 5),
                            Text("Yogyakarta", style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500),)
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(text, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.orange)),
                    Text(text2, style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.grey)),
                  ],
                )
              ],
            ),
          )
        ),
        SizedBox(height: 10,)
      ],
    );
  }
}