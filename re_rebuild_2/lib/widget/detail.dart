import 'package:flutter/material.dart';

// ignore: must_be_immutable
class DetailHouse extends StatefulWidget {
  String imageUrl;
  String name;
  String text1;
  String text2;
  DetailHouse({ Key? key, required this.imageUrl, required this.name, required this.text1, required this.text2}) : super(key: key);

  @override
  _DetailHouseState createState() => _DetailHouseState();
}

class _DetailHouseState extends State<DetailHouse> {
  int num = 0;
  String image = '';
  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.arrow_back, size: 25, color: Colors.black,),
            ),
            Text("Details", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.black)),
            SizedBox(width: 20,)
          ],
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(left: 15, right: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 20),
                Center(
                  child: Container(
                  height: 330,
                  width: widthScreen-20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Stack(
                      children: [
                        ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image.asset(
                              (num == 0) ? widget.imageUrl : image,
                                fit: BoxFit.fill,
                            ),
                        ),
                        Align(
                          alignment: Alignment.topRight,
                          child: Padding(
                            padding: EdgeInsets.only(right: 20, top: 20),
                            child: Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.white70
                              ),
                              child: Center(
                                child: Icon(Icons.favorite, size: 25, color: Colors.red,),
                              ),
                            ),
                          )
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: EdgeInsets.all(15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      num = 0;
                                      image = widget.imageUrl;
                                    });
                                  },
                                  child: Container(
                                    height: 80,
                                    width: 80,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: (num != 0) ? null : Colors.white70 ,
                                    ),
                                    child: Center(
                                      child: Container(
                                        height: 70,
                                        width: 70,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(10),
                                          child: Image.asset(
                                            widget.imageUrl,
                                              fit: BoxFit.fill,
                                          ),
                                        ),
                                      )
                                    ),
                                  ),
                                ),
                                SizedBox(width: 20),
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      num = 1;
                                      image = 'assets/living.jpg';
                                    });
                                  },
                                  child: Container(
                                    height: 80,
                                    width: 80,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: (num == 1) ? Colors.white70 : null,
                                    ),
                                    child: Center(
                                      child: Container(
                                        height: 70,
                                        width: 70,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(10),
                                          child: Image.asset(
                                            'assets/living.jpg',
                                              fit: BoxFit.fill,
                                          ),
                                        ),
                                      )
                                    ),
                                  ),
                                ),
                              ],
                            )
                          )
                        ),
                      ],
                    )
                  ),
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(widget.name, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),),
                            SizedBox(height: 5),
                            Row(
                              children: [
                                Image.asset('assets/maps.png', height: 17, width: 17, color: Colors.orange),
                                SizedBox(width: 5),
                                Text(widget.text1, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300),)
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(widget.text2, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.orange)),
                        Text('/month', style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.grey)),
                      ],
                    )
                  ],
                ),
                SizedBox(height: 20),
                Text("Facilities", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                SizedBox(height: 10),
                Container(
                  height: 100,
                  width: widthScreen-20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(width: 3, color: Colors.grey.shade300),
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(left: 10, top: 10, right: 10),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.asset('assets/bed.png', height: 25, width: 25),
                                SizedBox(width: 5),
                                Text("1 Bed", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold))
                              ],
                            ),
                            Row(
                              children: [
                                Image.asset('assets/shower.png', height: 25, width: 25),
                                SizedBox(width: 5),
                                Text("1 Bathroom", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold))
                              ],
                            ),
                            Row(
                              children: [
                                Image.asset('assets/cupboard.png', height: 25, width: 25),
                                SizedBox(width: 5),
                                Text("Kitchen", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold))
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.asset('assets/wifi.png', height: 25, width: 25),
                                SizedBox(width: 5),
                                Text("Wifi", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold))
                              ],
                            ),
                            Row(
                              children: [
                                Image.asset('assets/cctv.png', height: 25, width: 25),
                                SizedBox(width: 5),
                                Text("CCTV", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold))
                              ],
                            ),
                            SizedBox(width: 110,),
                          ],
                        )
                      ],
                    )
                  ),
                ),
                SizedBox(height: 20),
                Text("Descriptions", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                SizedBox(height: 10),
                Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco\nlaboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", 
                  style: TextStyle(fontSize: 13, fontWeight: FontWeight.w400),
                  textAlign: TextAlign.justify,),
                SizedBox(height: 20),
              ],
            ),
          )
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.all(10),
        child: SizedBox(
          width: widthScreen-30,
          height: 60,
          child: TextButton(
            style: TextButton.styleFrom(
              backgroundColor: Colors.orange,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30)
              )
            ),
            onPressed: () {
              //Navigator.push(context, MaterialPageRoute(builder: (context) => CheckOut()));
            },
            child: Text(
              "Rent Now",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      )
    );
  }
}