import 'package:flutter/material.dart';
import 'package:re_rebuild_2/home.dart';

class BottomNav extends StatefulWidget {
  const BottomNav({ Key? key }) : super(key: key);

  @override
  State<BottomNav> createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
int _selectedIndex = 0;
final halaman = [
  Home(),
  Home(),
  Home(),
  Home(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: halaman[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 5,),
              child: Image.asset(
                'assets/homepage.png',
                height: 20,
                width: 20,
                color: (_selectedIndex == 0) ? Colors.orange : Colors.grey,
              )
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 5,),
              child: Image.asset(
                'assets/message.png',
                height: 20,
                width: 20,
                color: (_selectedIndex == 1) ? Colors.orange : Colors.grey,
              ),
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 5,),
              child: Image.asset(
                'assets/heart.png',
                height: 20,
                width: 20,
                color: (_selectedIndex == 2) ? Colors.orange : Colors.grey,
              ),
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 5,),
              child: Image.asset(
                'assets/user.png',
                height: 20,
                width: 20,
                color: (_selectedIndex == 3) ? Colors.orange : Colors.grey,
              ),
            ),
            label: '',
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      )
    );
  }
}
